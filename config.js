const mysql = require('mysql')

module.exports = {
    Connect : mysql.createConnection({
        host:'localhost',
        user:'root',
        port:3307,
        password:'',
        database:'db_animeflix'
    })
}