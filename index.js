const request = require('request')
const connect = require('./config')
const cron = require('node-cron')

let date = new Date()
let d = date.getDay()

const sendMsg = (device , message) =>{
    let restKey = 'OWU4MzQ2NjctZDA1NC00ZTFhLThlNmQtM2Q3MGFhYjdjZDA0'
    let appId = '07d03c16-6f65-4330-8bad-4194b81483cb'
    request(
        {
            method:'POST',
            uri:'https://onesignal.com/api/v1/notifications',
            headers: {
                "authorization":`Basic ${restKey}`,
                "content-type":"application/json"
            },
            json:true,
            body:{
                'app_id':appId,
                'contents':{en: message},
                'include_player_ids': Array.isArray(device) ? device : [device]
            }
        },
        (err, response, body) => {
            if(!body.errors){
                console.log(body)
            }else{
                console.error('Error :',body.errors)
            }
        }
    )
}

const task = cron.schedule('* * * * * *', () => {
    connect.Connect.query(`SELECT * FROM favourites`,(errFav,resultFav,fieldFav) => {
        if (errFav) throw errFav
        resultFav.map((val,key) => {
            connect.Connect.query(`SELECT * FROM users WHERE id = ${val.id_user}`,(errUser,resultUser,fieldUser) => {
                if (errUser) throw errUser
                resultUser.map((vall,keyy) => {
                    connect.Connect.query(`SELECT COUNT(*) AS jumlah FROM videos WHERE series = '${val.name_series}'`,(errSeri,resultSeri,fieldSeri) => {
                        if (errSeri) throw errSeri
                            let jml = resultSeri[0].jumlah-1
                            sendMsg(vall.app_id, `Halo Animers !!! kami ingin ngasih tau kalo series ${val.name_series} favoritmu sedang di episode ${jml}`);
                    })
                })
            })    
        })
    })    
},{
    scheduled: false
})

connect.Connect.query(`SELECT COUNT(*) AS jumlah FROM users WHERE app_id != 'website'`,(err,result,field) => {
    let jml = result[0].jumlah
    for(i=1;i<=jml;i++){
      task.start()
    }
})

